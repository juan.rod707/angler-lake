﻿using System;
using Assets.Scripts.Fishing;

namespace Assets.Scripts.Player.Persistence
{
    [Serializable]
    public struct PlayerStats
    {
        public int TotalCatches => CarpCatches + MuskieCatches + BTroutCatches + GTroutCatches + WalleyeCatches +
                                   PikeCatches + BluegillCatches + WhitefishCatches + BassCatches;

        public int CarpCatches;
        public int MuskieCatches;
        public int BTroutCatches;
        public int GTroutCatches;
        public int WalleyeCatches;
        public int PikeCatches;
        public int BluegillCatches;
        public int WhitefishCatches;
        public int BassCatches;

        public TrophyFish[] Trophies;
    }

    [Serializable]
    public class TrophyFish
    {
        public string Id;
        public string Name;
        public Tier Tier;
        public float Weight;

        public static TrophyFish FromFish(Fish fish)
        {
            return new TrophyFish
            {
                Id = fish.Stats.Id,
                Name = fish.Stats.Name,
                Weight = fish.Stats.Weight,
                Tier = fish.Tier
            };
        }
    }
}
