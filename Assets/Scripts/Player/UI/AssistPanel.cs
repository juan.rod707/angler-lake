﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.Player.UI
{
    public class AssistPanel : MonoBehaviour
    {
        public Text SwitchModeText;
        public Text ActionText;
        public GameObject ActionAssistContainer;
        public GameObject ModeAssistContainer;
        public GameObject RankingAssistContainer;

        public string FishModeText;
        public string DriveModeText;
        public string ActionAimText;
        public string ActionFishingText;

        public void SwitchToFishMode()
        {
            SwitchModeText.text = FishModeText;
            ActionAssistContainer.SetActive(true);
            SwitchToAiming();
        }

        public void SwitchToDriveMode()
        {
            SwitchModeText.text = DriveModeText;
            ActionAssistContainer.SetActive(false);
        }

        public void SwitchToFishing()
        {
            ModeAssistContainer.SetActive(false);
            ActionText.text = ActionFishingText;
        }

        public void SwitchToAiming()
        {
            ModeAssistContainer.SetActive(true);
            ActionText.text = ActionAimText;
        }

        public void HideRankingAssist()
        {
            RankingAssistContainer.gameObject.SetActive(false);
        }

        public void ShowRankingAssist()
        {
            RankingAssistContainer.gameObject.SetActive(true);
        }
    }
}
