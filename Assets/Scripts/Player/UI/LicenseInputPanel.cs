﻿using System;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.Player.UI
{
    public class LicenseInputPanel : MonoBehaviour
    {
        public InputField NameInput;
        public Dropdown CountrySelector;
        public Sprite[] Flags;

        private Action<string, string> onSubmitted;

        public void Show(Action<string, string> onSubmitted)
        {
            gameObject.SetActive(true);
            this.onSubmitted = onSubmitted;

            LoadCountries();
        }

        private void LoadCountries()
        {
            CountrySelector.ClearOptions();
            CountrySelector.AddOptions(Flags.Select(f => f.name).Where(f => !f.Contains("flag")).ToList());
        }

        public void Submit()
        {
            if (NameIsValid(NameInput.text))
                onSubmitted(NameInput.text, CountrySelector.captionText.text);
        }

        private bool NameIsValid(string name)
        {
            return !string.IsNullOrEmpty(name) &&
                   !name.ToLower().Contains("nigger") &&
                   !name.ToLower().Contains("faggot");
        }

        public void Hide() => gameObject.SetActive(false);
    }
}
