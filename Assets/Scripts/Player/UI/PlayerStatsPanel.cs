﻿using System;
using Assets.Scripts.Common;
using Assets.Scripts.Common.UI;
using Assets.Scripts.Player.Persistence;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.Player.UI
{
    public class PlayerStatsPanel : MonoBehaviour
    {
        public Text PlayerName;
        public Text PlayerCountry;
        public CatchesPage CatchesPage;
        public TrophiesPage TrophiesPage;
        public PopUpPanel PopUp;
        public PagedPanel PagedPanel;
        private Action onClose;

        public void Initialize(Action onClose)
        {
            gameObject.SetActive(false);
            PagedPanel.Initialize();
            this.onClose = onClose;
        }

        public void Show(PlayerStats playerStats)
        {
            gameObject.SetActive(true);
            CatchesPage.FillData(playerStats);
            TrophiesPage.FillData(playerStats);
            PlayerName.text = PlayerPrefsHandler.GetPlayerName;
            PlayerCountry.text = PlayerPrefsHandler.GetCountry;
            PopUp.Open();
        }

        void Update()
        {
            if(Input.GetButtonDown("Cancel") || Input.GetButtonDown("Stats"))
                Close();

            if (Input.GetButtonDown("Previous") || Input.GetButtonDown("Next"))
                SwitchPage();
        }
        public void Close() => PopUp.Close(OnClosed);

        void OnClosed()
        {
            gameObject.SetActive(false);
            onClose();
        }

        void SwitchPage() => PagedPanel.NextPage();
    }
}
