﻿using System;
using System.Collections;
using System.Linq;
using Assets.PlayerCharacter;
using Assets.Scripts.Common;
using Assets.Scripts.Minigame.UI;
using Assets.Scripts.Pier;
using Assets.Scripts.Player;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Assets.Scripts.Fishing
{
    public class FishingDirector : MonoBehaviour
    {
        public FishingZone[] FishingZones;
        public FishParameters[] FishParameters;
        public MinigameMechanics Minigame;
        public MiniGamePanel MinigamePanel;
        public FeedbackSfx FeedbackSfx;
        public float RegenerationTime;
        public float BiteWindow;
        public float MinBiteDelay;
        public float MaxBiteDelay;
        public FishingDirectorTweaker Tweaker;

        private PlayerInput playerInput;
        private Character character;
        
        private Fish currentFish;
        private Action<Fish> onFishCaught;
        private Bobber bobber;
        

        public bool IsBiting { get; private set; }
        private bool IsFirstTimePlaying => PlayerPrefsHandler.IsFirstTimePlaying;

        public void Initialize(PlayerInput playerInput, Character character, Action<Fish> onFishCaught)
        {
            RankingsPersistence.LoadFishTypes(FishParameters.Select(fp => fp.Id).ToArray());
            this.onFishCaught = onFishCaught;
            this.character = character;
            this.playerInput = playerInput;

            bobber = character.FishingPole.Bobber;
            Minigame.Initialize(FishEscaped, FishCaught);
            MinigamePanel.gameObject.SetActive(false);
            RegenerateZones();
        }

        private void RegenerateZones()
        {
            foreach (var fz in FishingZones)
                fz.Regenerate();
            StartCoroutine(WaitAndDo(RegenerationTime, RegenerateZones));
        }

        public void OnFishing()
        {
            var time = Random.Range(MinBiteDelay, MaxBiteDelay);
            StartCoroutine(WaitAndDo(time, OnBite));
        }

        private void OnBite()
        {
            if (Tweaker.Enabled)
                currentFish = new Fish(Tweaker.Level, Tier.Common, FishParameters.First(fp => fp.Id.Equals(Tweaker.FishId)));
            else
                currentFish = ClosestZoneTo(bobber.Position).GetNewFish();

            StartCoroutine(StartBiteWindow(currentFish));
            character.NotifyBite();
            Debug.Log($"Got a bite: {currentFish.Stats.Name}, Tier: {currentFish.Tier}, {currentFish.Stats.Weight.ToString("0.00")}Lbs");
        }

        private FishingZone ClosestZoneTo(Vector3 bobberPosition) =>
            FishingZones.Select(fz => new {zone = fz, dist = Vector3.Distance(fz.transform.position, bobberPosition)})
                .OrderBy(fzd => fzd.dist)
                .First().zone;

        public void AttemptCatch() => EnableMinigame(currentFish);

        void FishEscaped()
        {
            DisableMinigame();
            FeedbackSfx.OnEscape();
        }

        void FishCaught(Fish fish)
        {
            ShowCatchReport(fish);
            onFishCaught(fish);
            switch (fish.Tier)
            {
                case Tier.Common:
                    FeedbackSfx.OnCommonCatch();
                    break;
                case Tier.Uncommon:
                    FeedbackSfx.OnUncommonCatch();
                    break;
                case Tier.Rare:
                    FeedbackSfx.OnRareCatch();
                    break;
                case Tier.Epic:
                    FeedbackSfx.OnEpicCatch();
                    break;
            }
        }

        private void ShowCatchReport(Fish fish)
        {
            Minigame.StopMinigame();
            MinigamePanel.ShowCatchReport(fish);
            StartCoroutine(WaitAndDo(2f, DisableMinigame));
        }

        private IEnumerator StartBiteWindow(Fish fish)
        {
            IsBiting = true;
            yield return new WaitForSeconds(BiteWindow);
            IsBiting = false;
            bobber.FishEscaped();
        }

        void DisableMinigame()
        {
            Minigame.StopMinigame();
            MinigamePanel.Hide();
            playerInput.enabled = true;
            character.ResetCast();
        }

        void EnableMinigame(Fish fish)
        {
            if (IsFirstTimePlaying)
            {
                PlayerPrefs.SetInt("FTUX", 1);
                MinigamePanel.ShowWithTutorial(StartMinigame);
            }
            else
                MinigamePanel.Show(StartMinigame);

            Minigame.Prepare(fish);
            playerInput.enabled = false;
        }

        void StartMinigame() => Minigame.StartMinigame();

        public void CancelFishing() => StopAllCoroutines();

        private IEnumerator WaitAndDo(float time, Action onWaited)
        {
            yield return new WaitForSeconds(time);
            onWaited();
        }
    }
}
