﻿using System;
using Random = UnityEngine.Random;

namespace Assets.Scripts.Fishing
{
    [Serializable]
    public struct FishTier
    {
        public Tier Tier;
        public float MinLevel;
        public float MaxLevel;

        public float RandomLevel => Random.Range(MinLevel, MaxLevel);
    }

    public enum Tier
    {
        Common,
        Uncommon,
        Rare,
        Epic
    }
}
