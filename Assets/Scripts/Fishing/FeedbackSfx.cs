﻿using System;
using UnityEngine;

namespace Assets.Scripts.Fishing
{
    [Serializable]
    public class FeedbackSfx
    {
        public AudioSource Escape;
        public AudioSource CommonCatch;
        public AudioSource UncommonCatch;
        public AudioSource RareCatch;
        public AudioSource EpicCatch;

        public void OnEscape() => Escape.Play();
        public void OnCommonCatch() => CommonCatch.Play();
        public void OnUncommonCatch() => UncommonCatch.Play();
        public void OnRareCatch() => RareCatch.Play();
        public void OnEpicCatch() => EpicCatch.Play();
    }
}
