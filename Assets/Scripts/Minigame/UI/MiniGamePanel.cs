﻿using System;
using System.Collections;
using Assets.Scripts.Common;
using Assets.Scripts.Fishing;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.Minigame.UI
{
    public class MiniGamePanel : MonoBehaviour
    {
        public Transform AnalogIndicator;
        public Transform RingZone;
        public Image FishZone;
        public Image FishIcon;
        public Slider ProgressSlider;
        public Image LineStrengthMeter;
        public Gradient LineStrengthGradient;
        public Animator Animator;
        public Color DisabledFishColor;
        public Color EnabledFishColor;
        public CatchReport CatchReport;
        public GameObject Tutorial;

        private float baseLineStrength;

        public void UpdateAnalogIndicator(Vector2 analogDirection) =>
            AnalogIndicator.eulerAngles = MathHelper.EulerFromDirection(analogDirection);

        public void UpdateZoneIndicator(Vector2 safeZoneStart) =>
            RingZone.eulerAngles = MathHelper.EulerFromDirection(safeZoneStart);

        public void UpdateProgress(float progress) =>
            ProgressSlider.value = progress;

        public void UpdateLineDegradation(float lineStrength) =>
            LineStrengthMeter.color = LineStrengthGradient.Evaluate(lineStrength / baseLineStrength);

        public void Initialize(float fishZone, float progressGoal, float baseLineStrength, float currentProgress)
        {
            ProgressSlider.maxValue = progressGoal;
            this.baseLineStrength = baseLineStrength;
            FishZone.fillAmount = fishZone / 360f;
            UpdateLineDegradation(this.baseLineStrength);
            UpdateProgress(currentProgress);
        }

        public void Hide()
        {
            Animator.SetTrigger("Exit");
            StartCoroutine(WaitandDo(1f, () => { gameObject.SetActive(false); }));
        }

        public void Show(Action onAnimationFinished)
        {
            gameObject.SetActive(true);
            FishIcon.color = DisabledFishColor;
            StartCoroutine(WaitandDo(.5f, () =>
            {
                FishIcon.color = EnabledFishColor;
                onAnimationFinished();
            }));
        }

        public void ShowWithTutorial(Action onAnimationFinished)
        {
            gameObject.SetActive(true);
            Tutorial.SetActive(true);
            FishIcon.color = DisabledFishColor;
            StartCoroutine(WaitandDo(20f, () =>
            {
                FishIcon.color = EnabledFishColor;
                Tutorial.SetActive(false);
                onAnimationFinished();
            }));
        }

        private IEnumerator WaitandDo(float time, Action onFinished)
        {
            yield return new WaitForSeconds(time);
            onFinished();
        }

        public void ShowCatchReport(Fish fish)
        {
            Animator.SetTrigger("ShowReport");
            CatchReport.Populate(fish.Stats.Name, fish.Tier, fish.Stats.Weight, fish.Stats.Photo);
        }
    }
}