﻿using System;
using Assets.Scripts.Fishing;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.Minigame.UI
{
    public class CatchReport : MonoBehaviour
    {
        public Image FishView;
        public Text FishName;
        public Text FishDetails;

        public string DetailFormat;

        public void Populate(string name, Tier tier, float weight, Sprite photo)
        {
            FishName.text = name;
            FishDetails.text = String.Format(DetailFormat, tier.ToString(), weight.ToString("0.00"));
            FishView.sprite = photo;
        }
    }
}
