﻿using System;
using Assets.Scripts.Fishing;
using Assets.Scripts.Minigame;
using Assets.Scripts.Minigame.UI;
using UnityEngine;

public class MinigameMechanics : MonoBehaviour
{
    public MiniGamePanel UIPanel;
    public FishMovement FishMovement;
    public MinigameSfx Sfx;

    public float CaptureProgressGoal;
    public float BaseCaptureProgress;
    public float BaseLineStrength;
    public float LineRecovery;

    private Vector2 safeZoneStart;
    private Vector2 safeZoneEnd;
    private Vector2 analog;
    private float captureProgress;
    private float lineStrength;
    private Fish fish;
    private Action onFishEscaped;
    private Action<Fish> onFishCaught;

    bool IsInSafeZone =>
        analog.magnitude > 0 &&
        Vector2.Angle(safeZoneStart, analog) < fish.Stats.FishZone
        && Vector2.Angle(safeZoneEnd, analog) < fish.Stats.FishZone;

    public void Initialize(Action onFishEscaped, Action<Fish> onFishCaught)
    {
        this.onFishEscaped = onFishEscaped;
        this.onFishCaught = onFishCaught;
        StopMinigame();
    }

    public void Prepare(Fish fish)
    {
        this.fish = fish;
        ResetMinigame();
    }

    public void StartMinigame()
    {
        FishMovement.Initialize(fish, Sfx.FishChangeSfx.Play);
        this.enabled = true;
    }

    public void StopMinigame()
    {
        this.enabled = false;
        FishMovement.Stop();
    }

    public void ResetMinigame()
    {
        UIPanel.Initialize(fish.Stats.FishZone, CaptureProgressGoal, BaseLineStrength, BaseCaptureProgress);
        captureProgress = BaseCaptureProgress;
        lineStrength = BaseLineStrength;
    }

    void FixedUpdate()
    {
        ReadAnalog();
        ReadMouse();
        CalculateArc();
        CalculateCaptureProgress();
        UpdateUI();

        if(Input.GetButtonDown("Cancel"))
            onFishEscaped();
    }

    private void UpdateUI()
    {
        UIPanel.UpdateAnalogIndicator(analog);
        UIPanel.UpdateZoneIndicator(safeZoneStart);
        UIPanel.UpdateProgress(captureProgress);
        UIPanel.UpdateLineDegradation(lineStrength);
    }

    private void CalculateCaptureProgress()
    {
        if (Input.GetButton("Action"))
        {
            if (IsInSafeZone)
                UpdateCaptureProgress(fish.Stats.CatchRate);
            else
                UpdateLineStrength(-fish.Stats.LineDegradation);
        }
        else
        {
            UpdateCaptureProgress(-fish.Stats.EscapeRate);
            UpdateLineStrength(LineRecovery);
        }
    }

    private void UpdateCaptureProgress(float deltaProgress)
    {
        captureProgress += deltaProgress;

        if (captureProgress > CaptureProgressGoal)
            onFishCaught(fish);

        if (captureProgress < 0)
        {
            captureProgress = -.1f;
            UpdateLineStrength(-fish.Stats.LineDegradation * 2f);
        }

        if (deltaProgress > 0)
            Sfx.ReelInSfx.Play();
        else if (deltaProgress < 0)
            Sfx.ReelIdleSfx.Play();
    }

    private void UpdateLineStrength(float deltaStrength)
    {
        lineStrength += deltaStrength;

        if (lineStrength > BaseLineStrength)
            lineStrength = BaseLineStrength;

        if (lineStrength < 0)
            onFishEscaped();

        if(deltaStrength < 0)
            Sfx.LineTensionSfx.Play();

    }

    private void ReadAnalog() => 
        analog = new Vector2(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"));

    private void ReadMouse()
    {
        if (analog.magnitude <= 0)
        {
            var mouse = Input.mousePosition;
            mouse.x -= Screen.width / 2f;
            mouse.y -= Screen.height / 2f;

            analog = mouse.normalized;
        }
    }

    private void CalculateArc()
    {
        var halfAngle = fish.Stats.FishZone * Mathf.Deg2Rad;
        safeZoneStart = transform.right;
        safeZoneEnd = transform.TransformVector(new Vector2(Mathf.Cos(halfAngle), -Mathf.Sin(halfAngle)));
    }
}



