﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Assets.Scripts.Common
{
    public class WeightedList<T>
    {
        private readonly IEnumerable<T> distributionList;

        public WeightedList(IEnumerable<WeightedItem<T>> distibutionList)
        {
            var items = new List<T>();
            foreach (var i in distibutionList)
                foreach (var _ in Enumerable.Range(0, i.Weight))
                    items.Add(i.Item);

            distributionList = items;
        }


        public T GetRandomItem() => 
            distributionList.PickOne();
    }

    [Serializable]
    public class WeightedItem<T>
    {
        public T Item;
        public int Weight;
    }
}
