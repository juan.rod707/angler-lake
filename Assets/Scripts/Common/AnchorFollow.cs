﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Assets.Scripts.Common
{
    public class AnchorFollow : MonoBehaviour
    {
        public float Speed;
        public Transform Target;

        void Update() => 
            transform.position = Vector3.Lerp(transform.position, Target.position, Speed * Time.deltaTime);
    }
}
