﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Assets.Scripts.Common
{
    public static class MathHelper
    {
        public static Vector3 EulerFromDirection(Vector2 targetDirection) =>
            new Vector3(0, 0, Vector3.SignedAngle(Vector3.up, targetDirection, Vector3.forward));
    }
}
