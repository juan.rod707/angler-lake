﻿using System.Linq;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.Common.UI
{
    public class InputAssistText : MonoBehaviour
    {
        [TextArea(1,5)]
        public string KeyboardText;
        [TextArea(1, 5)]
        public string ControllerText;

        private bool ControllerConnected => Input.GetJoystickNames().Any() &&
                                            !string.IsNullOrEmpty(Input.GetJoystickNames().First());

        void Start()
        {
            if (ControllerConnected)
                GetComponent<Text>().text = ControllerText;
            else
                GetComponent<Text>().text = KeyboardText;
        }
    }
}
