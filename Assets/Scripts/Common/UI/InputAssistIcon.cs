﻿using System.Linq;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.Common.UI
{
    public class InputAssistIcon : MonoBehaviour
    {
        public Sprite KeyboardIcon;
        public Sprite ControllerIcon;

        private bool ControllerConnected => Input.GetJoystickNames().Any() && 
                                            !string.IsNullOrEmpty(Input.GetJoystickNames().First());

        void Start()
        {
            if (ControllerConnected)
                GetComponent<Image>().sprite = ControllerIcon;
            else
                GetComponent<Image>().sprite = KeyboardIcon;
        }
    }
}
