﻿using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.Common.UI
{
    public class PagedPanel : MonoBehaviour
    {
        public Text Title;
        public AudioSource ChangePageSfx;

        private Page[] pages;
        private int pageIndex;

        public void Initialize()
        {
            pageIndex = 0;
            pages = GetComponentsInChildren<Page>(true);
            HideAllPages();
            DispayCurrentPage();
        }

        private void HideAllPages()
        {
            foreach (var p in pages) p.Hide();
        }

        public void NextPage()
        {
            pageIndex++;
            if (pageIndex >= pages.Length)
                pageIndex = 0;

            HideAllPages();
            DispayCurrentPage();
            ChangePageSfx?.Play();
        }

        public void PreviousPage()
        {
            pageIndex--;
            if (pageIndex < 0)
                pageIndex = pages.Length - 1;

            HideAllPages();
            DispayCurrentPage();
            ChangePageSfx?.Play();
        }

        void DispayCurrentPage()
        {
            pages[pageIndex].Show();
            Title.text = pages[pageIndex].Title;
        }
    }
}
