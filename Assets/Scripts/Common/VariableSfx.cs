﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Assets.Scripts.Common
{
    public class VariableSfx : MonoBehaviour
    {
        public AudioSource Sfx;
        public float MinPitch;
        public float MaxPitch;

        public void Play()
        {
            if (!Sfx.isPlaying)
            {
                Sfx.pitch = Random.Range(MinPitch, MaxPitch);
                Sfx.Play();
            }
        }
    }
}
