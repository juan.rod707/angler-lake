﻿using System.Linq;
using UnityEngine;

namespace Assets.Scripts.Common
{
    public class MusicPlayer : MonoBehaviour
    {
        void Awake()
        {
            DontDestroyOnLoad(gameObject);
        }

        void Start()
        {
            var musicPlayers = FindObjectsOfType<MusicPlayer>();
            if(musicPlayers.Length > 1)
                Destroy(musicPlayers.Last().gameObject);
        }
    }
}
