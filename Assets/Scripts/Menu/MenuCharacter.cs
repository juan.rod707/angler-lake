﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Assets.Scripts.Menu
{
    public class MenuCharacter : MonoBehaviour
    {
        public LineRenderer Line;
        public Transform Bobber;

        void Update() => Line.SetPositions(new[] {Line.transform.position, Bobber.position});
    }
}
