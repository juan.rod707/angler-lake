﻿using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.UI;

namespace Assets.Scripts.Menu
{
    public class OptionsMenu : MonoBehaviour
    {
        public AudioMixer MainMixer;
        public Slider MusicSlider;
        public Slider EffectsSlider;
        public Slider AmbianceSlider;

        void Start()
        {
            var musicLevel = PlayerPrefs.GetFloat("musicLevel", 0);
            MusicSlider.value = musicLevel;
            SetMusicLevel();

            var effectsLevel = PlayerPrefs.GetFloat("effectsLevel", 0);
            EffectsSlider.value = effectsLevel;
            SetEffectsLevel();

            var ambianceLevel = PlayerPrefs.GetFloat("ambianceLevel", 0);
            AmbianceSlider.value = ambianceLevel;
            SetAmbianceLevel();
        }

        public void SetMusicLevel()
        {
            var musicLevel = MusicSlider.value;
            SetChannelLevel("MusicLevel", musicLevel);

            PlayerPrefs.SetFloat("musicLevel", musicLevel);
            PlayerPrefs.Save();
        }

        public void SetEffectsLevel()
        {
            var effectsLevel = EffectsSlider.value;
            SetChannelLevel("EffectsLevel", effectsLevel);

            PlayerPrefs.SetFloat("effectsLevel", effectsLevel);
            PlayerPrefs.Save();
        }

        public void SetAmbianceLevel()
        {
            var ambianceLevel = AmbianceSlider.value;
            SetChannelLevel("AmbianceLevel", ambianceLevel);

            PlayerPrefs.SetFloat("ambianceLevel", ambianceLevel);
            PlayerPrefs.Save();
        }

        void SetChannelLevel(string channelName, float level) => MainMixer.SetFloat(channelName, level);

    }
}
