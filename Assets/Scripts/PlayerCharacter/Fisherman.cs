﻿using UnityEngine;

namespace Assets.PlayerCharacter
{
    public class Fisherman : MonoBehaviour
    {
        private Cursor cursor;
        private bool isDriving;

        public void Initialize(Cursor cursor) => this.cursor = cursor;

        public void LockOnCursor() => isDriving = false;

        public void ResetToBoat()
        {
            isDriving = true;
            transform.localRotation = Quaternion.identity;
        }

        void Update()
        {
            if (!isDriving)
            {
                transform.LookAt(cursor.transform);
                NormalizePitchAndRoll();
            }
        }

        void NormalizePitchAndRoll()
        {
            var source = transform.eulerAngles;
            transform.eulerAngles = new Vector3(0f, source.y, 0f);
        }
    }
}
