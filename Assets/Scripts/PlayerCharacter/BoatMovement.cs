﻿using UnityEngine;
using UnityEngine.AI;

namespace Assets.PlayerCharacter
{
    public class BoatMovement : MonoBehaviour
    {
        public float Speed;
        public AudioSource EngineSfx;

        public float BaseEnginePitch;
        public float EnginePitchFactor;

        private NavMeshAgent agent;

        public bool IsEnabled { get; private set; }

        public void Initialize(NavMeshAgent agent)
        {
            this.agent = agent;
            agent.speed = Speed;
            EngineSfx.pitch = BaseEnginePitch;
        }

        public void Move(Vector2 movementVector)
        {
            if (IsEnabled)
            {
                var targetMoveVector = new Vector3(movementVector.x, 0f, movementVector.y);
                agent.SetDestination(transform.position + targetMoveVector);

                EngineSfx.pitch = BaseEnginePitch + agent.velocity.magnitude * EnginePitchFactor;
            }
        }

        public void Enable()
        {
            IsEnabled = true;
            EngineSfx.pitch = BaseEnginePitch;
        }

        public void Disable()
        {
            IsEnabled = false;
            EngineSfx.pitch = 0;
        }
    }
}
