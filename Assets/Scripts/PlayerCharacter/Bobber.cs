﻿using System;
using Assets.Scripts.Fishing;
using UnityEngine;

namespace Assets.PlayerCharacter
{
    public class Bobber : MonoBehaviour
    {
        public Animator Animator;
        public ParticleSystem RippleVfx;
        public ParticleSystem SplashVfx;
        public ParticleSystem FastRippleVfx;
        public Transform BobberMesh;
        public ParticleSystem NotificationVfx;
        public AudioSource SlpashSfx;

        public Vector3 Position => transform.position;
        public Vector3 AnimatedPosition => BobberMesh.transform.position;

        private Action onFishing;

        public void Initialize(Action onFishing) => this.onFishing = onFishing;

        public void Reset()
        {
            RippleVfx.Stop();
            Animator.enabled = false;
            transform.rotation = Quaternion.identity;
        }

        public void OnWater(float waterLevel)
        {
            SplashVfx.Play();
            RippleVfx.Play();
            SlpashSfx.Play();
            Animator.enabled = true;
            Animator.SetBool("IsBiting", false);

            var pos = transform.position;
            pos.y = waterLevel;
            transform.position = pos;

            onFishing();
        }

        public void SetPosition(Vector3 target) => 
            transform.position = target;

        public void NotifyBite()
        {
            Animator.SetBool("IsBiting", true);
            NotificationVfx.Play();
            FastRippleVfx.Play();
        }

        public void FishEscaped()
        {
            Animator.SetBool("IsBiting", false);
            FastRippleVfx.Stop();
        }
    }
}
