﻿using UnityEngine;

namespace Assets.PlayerCharacter
{
    public class Cursor : MonoBehaviour
    {
        public SpriteRenderer Sprite;
        private float maxDistance;

        public void MoveTo(Vector3 movementVector)
        {
            transform.position += movementVector;
            transform.localPosition = Vector3.ClampMagnitude(transform.localPosition, maxDistance);
        }

        public void Show() => Sprite.enabled = true;
        public void Hide() => Sprite.enabled = false;

        public void Initialize(float maxDistance) => 
            this.maxDistance = maxDistance;
    }
}
