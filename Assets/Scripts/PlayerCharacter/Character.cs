﻿using System;
using Assets.Scripts.Fishing;
using Assets.Scripts.Player.UI;
using UnityEngine;
using UnityEngine.AI;

namespace Assets.PlayerCharacter
{
    public class Character : MonoBehaviour
    {
        public int BaseBaitCount;
        public BoatMovement BoatMovement;
        public CursorMovement CursorMovement;
        public Fisherman Fisherman;
        public FishermanView FishermanView;
        public FishingPole FishingPole;
        public AudioSource BiteNotificationSfx;
        public AudioSource CastSfx;

        public NavMeshAgent Agent;
        public Cursor Cursor;

        private int baitCount;
        private bool isFishing;
        private FishingDirector fishingDirector;
        private Action<string[]> updateBaitCount;
        private AssistPanel assistPanel;

        public void Initialize(AssistPanel assistPanel, FishingDirector fishingDirector, Action<string[]> updateBaitCount)
        {
            this.assistPanel = assistPanel;
            this.updateBaitCount = updateBaitCount;
            this.fishingDirector = fishingDirector;

            BoatMovement.Initialize(Agent);
            CursorMovement.Initialize(Cursor);
            Fisherman.Initialize(Cursor);
            FishingPole.Initialize(fishingDirector.OnFishing);

            BoatMovement.Enable();
            CursorMovement.Disable();
            Fisherman.ResetToBoat();

            assistPanel.SwitchToDriveMode();
            ResetBait();
        }

        private void UpdateBait() => updateBaitCount(new[] { baitCount.ToString() });

        public void SwitchMode()
        {
            if (!isFishing)
            {
                if (BoatMovement.IsEnabled)
                {
                    BoatMovement.Disable();
                    CursorMovement.Enable();
                    Fisherman.LockOnCursor();
                    FishermanView.Aim();
                    assistPanel.SwitchToFishMode();
                }
                else
                {
                    CursorMovement.Disable();
                    BoatMovement.Enable();
                    Fisherman.ResetToBoat();
                    FishermanView.Drive();
                    assistPanel.SwitchToDriveMode();
                }
            }
        }

        public void Action()
        {
            if (isFishing)
            {
                if (fishingDirector.IsBiting)
                    fishingDirector.AttemptCatch();
                else
                    ResetCast();

            }
            else if (CursorMovement.IsEnabled && baitCount > 0)
                Cast();
        }

        private void Cast()
        {
            FishingPole.StartCast(Cursor.transform.position);
            CursorMovement.Disable();
            isFishing = true;
            FishermanView.Cast();
            CastSfx.Play();
            assistPanel.SwitchToFishing();
        }

        public void ResetCast()
        {
            if (!BoatMovement.IsEnabled)
            {
                FishingPole.ResetCast();
                isFishing = false;
                CursorMovement.Enable();
                FishermanView.Aim();
                fishingDirector.CancelFishing();
                assistPanel.SwitchToAiming();
            }
        }

        public void NotifyBite()
        {
            FishingPole.Bobber.NotifyBite();
            BiteNotificationSfx.Play();
            baitCount--;
            UpdateBait();
        }

        public void ResetBait()
        {
            baitCount = BaseBaitCount;
            UpdateBait();
        }
    }
}
