﻿using System;
using System.Collections;
using UnityEngine;

namespace Assets.PlayerCharacter
{
    public class FishingPole : MonoBehaviour
    {
        public LineRenderer LineRenderer;
        public Bobber Bobber;
        public float WaterLevel;
        public float CastDelay;
        public float CastSpeed;
        private Vector3 LaunchPosition => LineRenderer.transform.position;
        private bool isCasted;

        public void Initialize(Action onFishing)
        {
            Bobber.Initialize(onFishing);
            ResetCast();
        }

        void Update()
        {
            if(isCasted)
                LineRenderer.SetPositions(new [] {LaunchPosition, Bobber.AnimatedPosition});
        }

        public void StartCast(Vector3 targetPosition)
        {
            isCasted = true;
            StartCoroutine(WaitAndLaunchBobber(targetPosition));
        }

        void CastBobber(Vector3 targetPosition)
        {
            LineRenderer.enabled = true;
            Bobber.gameObject.SetActive(true);
            Bobber.transform.SetParent(null);
            Bobber.Reset();
            StartCoroutine(LaunchBobber(targetPosition));
        }

        public void ResetCast()
        {
            isCasted = false;
            Bobber.SetPosition(LaunchPosition);
            LineRenderer.enabled = false;
            Bobber.transform.SetParent(transform);
            Bobber.gameObject.SetActive(false);
        }

        IEnumerator WaitAndLaunchBobber(Vector3 targetPosition)
        {
            yield return new WaitForSeconds(CastDelay);
            CastBobber(targetPosition);
        }

        IEnumerator LaunchBobber(Vector3 target)
        {
            var elapsed = 0f;
            var throwDistance = Vector3.Distance(target, LaunchPosition);
            var deltaThrow = CastSpeed / throwDistance  * Time.deltaTime;
            var trajectoryCurve = CalculateThrowCurve(throwDistance);

            while (Bobber.transform.position.y > WaterLevel)
            {
                Bobber.SetPosition(Trajectory(trajectoryCurve, elapsed, target - LaunchPosition));
                elapsed += deltaThrow;
                yield return null;
            }

            Bobber.OnWater(WaterLevel);
        }

        private AnimationCurve CalculateThrowCurve(float throwDistance)
        {
            var curve = new AnimationCurve();
            var firstKey = new Keyframe(0, LaunchPosition.y, -10, 10);
            curve.AddKey(firstKey);

            curve.AddKey(0.4f, LaunchPosition.y + throwDistance / 8f);

            var lastKey = new Keyframe(1, WaterLevel, 10, -10);
            curve.AddKey(lastKey);
            curve.AddKey(1.1f, WaterLevel - 3);

            return curve;
        }

        Vector3 Trajectory(AnimationCurve curve, float progress, Vector3 vector)
        {
            var y = curve.Evaluate(progress);
            var progressVector = (vector * progress) + LaunchPosition;
            return new Vector3(progressVector.x, y, progressVector.z);
        }
    }
}
