﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Assets.Scripts.Fishing;
using Assets.Scripts.Pier.UI;
using Assets.Scripts.Player;
using Assets.Scripts.Player.UI;
using Assets.Scripts.Web;
using UnityEngine;

namespace Assets.Scripts.Pier
{
    public class Pier : MonoBehaviour
    {
        private Action onDock;
        private Action onUndock;
        public AudioSource BellsSfx;
        public RecordContainer RecordPanel;
        public float DataFetchInterval;

        private App42DataManager dataManager;
        private FishBucket bucket;

        public void Initialize(Action onDock, Action onUndock, FishBucket bucket)
        {
            this.bucket = bucket;
            this.onDock = onDock;
            this.onUndock = onUndock;
            dataManager = new App42DataManager();
            dataManager.GetRecords(SaveLoadedRankings);
        }

        private void SaveLoadedRankings(IEnumerable<RecordDTO> rankings)
        {
            var dictionary = new Dictionary<string, List<RecordDTO>>();
            foreach (var r in rankings)
            {
                if (!dictionary.ContainsKey(r.FishId))
                    dictionary.Add(r.FishId, new List<RecordDTO>(new[] {r}));
                else
                    dictionary[r.FishId].Add(r);
            }

            RankingsPersistence.LoadWorldRecords(dictionary);

            RecordPanel.ReloadRecords();
            StartCoroutine(WaitAndFetchData());
        }

        private void OnTriggerEnter(Collider other)
        {
            onDock();
            BellsSfx.Play();
            CheckBucket();
            bucket.EmptyBucket();
        }

        private void OnTriggerExit(Collider other) => onUndock();

        IEnumerator WaitAndFetchData()
        {
            yield return new WaitForSeconds(DataFetchInterval);
            dataManager.GetRecords(SaveLoadedRankings);
        }

        void CheckBucket()
        {
            var topCatches = new List<Fish>();

            foreach (var f in bucket.Catches)
            {
                var topTen = RankingsPersistence.WorldRecords[f.Stats.Id].Take(10);
                if(topTen.Count() < 10)
                    topCatches.Add(f);
                else if (topTen.Any(tf => tf.Weight < f.Stats.Weight))
                    topCatches.Add(f);
            }

            foreach (var tc in topCatches)
            {
                var record = RecordDTO.FromFish(tc);
                RankingsPersistence.WorldRecords[tc.Stats.Id].Add(record);
                dataManager.AddRecord(record);
            }

            RecordPanel.ReloadRecords();
        }
    }
}
