﻿using System.Collections.Generic;
using Assets.Scripts.Web;

namespace Assets.Scripts.Pier
{
    public class RankingsPersistence
    {
        private static string[] fishIds;
        public static Dictionary<string, List<RecordDTO>> WorldRecords { get; private set; }

        public static void LoadFishTypes(string[] types) => fishIds = types;

        public static void LoadWorldRecords(Dictionary<string, List<RecordDTO>> dictionary)
        {
            WorldRecords = new Dictionary<string, List<RecordDTO>>();
            foreach (var fi in fishIds)
                WorldRecords.Add(fi, new List<RecordDTO>());

            foreach (var kvp in dictionary)
                WorldRecords[kvp.Key].AddRange(kvp.Value);
        }
    }
}
