﻿using System;
using System.Collections.Generic;
using System.Linq;
using Assets.Scripts.Common.UI;
using JetBrains.Annotations;
using UnityEngine;

namespace Assets.Scripts.Pier.UI
{
    public class RecordContainer : MonoBehaviour
    {
        public PopUpPanel PopUp;
        public RecordItem RecordItemPrefab;
        public PagedPanel PagedPanel;
        public GameObject ConnectionError;

        private IEnumerable<RankingPage> rankingPages;

        private Action onClose;

        public void Initialize(Action onClose)
        {
            PagedPanel.Initialize();
            gameObject.SetActive(false);
            this.onClose = onClose;
            rankingPages = GetComponentsInChildren<RankingPage>(true);
        }

        public void ReloadRecords()
        {
            if (RankingsPersistence.WorldRecords != null)
            {
                ConnectionError.SetActive(false);
                ClearAllPages();
                foreach (var fid in RankingsPersistence.WorldRecords.Keys)
                {
                    var fishRanking = RankingsPersistence.WorldRecords[fid].OrderByDescending(rd => rd.Weight).Take(10)
                        .ToArray();

                    for (int i = 0; i < fishRanking.Length; i++)
                    {
                        var data = fishRanking[i];
                        var listItem =
                            Instantiate(RecordItemPrefab,
                                rankingPages.First(rp => rp.FishId.Equals(data.FishId)).transform);
                        listItem.Populate(i + 1, data);
                    }
                }
            }
            else
                ConnectionError.SetActive(true);
        }

        private void ClearAllPages()
        {
            foreach (var rp in rankingPages)
                rp.Clear();
        }
        
        public void Show()
        {
            gameObject.SetActive(true);
            PopUp.Open();
        }

        public void Close() => PopUp.Close(OnClosed);

        void OnClosed()
        {
            gameObject.SetActive(false);
            onClose();
        }

        void Update()
        {
            if (Input.GetButtonDown("Cancel") || Input.GetButtonDown("Ranking"))
                Close();

            if (Input.GetButtonDown("Next"))
                PagedPanel.NextPage();
            else if (Input.GetButtonDown("Previous"))
                PagedPanel.PreviousPage();
        }
    }
}
