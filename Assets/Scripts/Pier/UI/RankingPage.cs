﻿using Assets.Scripts.Common.UI;

namespace Assets.Scripts.Pier.UI
{
    public class RankingPage : Page
    {
        public string FishId;

        public void Clear()
        {
            var items = GetComponentsInChildren<RecordItem>(true);
            foreach (var i in items)
                Destroy(i.gameObject);
        }
    }
}
