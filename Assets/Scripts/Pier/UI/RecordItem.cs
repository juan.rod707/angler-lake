﻿using System.Linq;
using Assets.Scripts.Common;
using Assets.Scripts.Web;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.Pier.UI
{
    public class RecordItem : MonoBehaviour
    {
        public FormattedText Rank;
        public Image Medal;
        public Image Flag;
        public Text Name;
        public FormattedText Weight;
        public Text Date;
        public Image BackgroundHighlight;

        public Sprite[] Medals;
        public Sprite[] Flags;

        public void Populate(int rank, RecordDTO data)
        {
            Rank.SetFormattedText(rank.ToString());
            SetMedal(rank);
            Flag.sprite = Flags.First(f => f.name.Equals(data.Country));
            Name.text = data.Name;
            Weight.SetFormattedText(new[] {data.Weight.ToString("0.00")});
            Date.text = data.CatchDate;
            BackgroundHighlight.enabled = data.PlayerId.Equals(PlayerPrefsHandler.GetPlayerId);
        }

        private void SetMedal(int rank)
        {
            if (rank < 4)
                Medal.sprite = Medals[rank - 1];
            else
                Medal.enabled = false;
        }
    }
}
